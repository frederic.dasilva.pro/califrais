# Backend dev technical test

This test is composed of two parts, a guided test, which will make you build a CRUD step by step, and a freeform test, which will let you solve a more complex problem. Do not hesitate to install bundles when you feel like they are needed (but avoid installing bundles that solve the whole problem, like EasyAdmin). The base for this test is a `symfony/website-skeleton` template with some pre-existing entities (Product, Supplier).

Prerequisite: A MySQL 5.7 server should be installed locally or reachable from your computer. 
## Guided test
 
1. Run the website (you can use the command “symfony server:start”). Create the database.
2. Create a non-nullable relation between products (Product entity) and suppliers (Supplier entity).
3. Find a way to track when the Product and Supplier entities are created and updated.
4. Add the “siret” field to the Supplier entity (not nullable). This field must contain a [standard SIRET code](https://en.wikipedia.org/wiki/SIRET_code) (14 digits only, do not handle exceptions like "MONACOCONFO001")
5. Fill the database with 100 random products and 20 random suppliers. All products should have a supplier.
6. Create a route ("/product/new"). Create a form to add new Products that lets the user fill every field in the entity (name, price, supplier) AND also create the supplier if it doesn’t exist.
7. Make sure that the user input is compatible with the fields: names shouldn’t be empty, prices shouldn’t be equal to zero, the email should be formally valid and the siret should be a 14 digits number valid with regard to Luhn's formula.
8. Create a route (“/product”) that returns a list of all the Products (show the columns: name, price, date created, date updated, supplier name, supplier siret, supplier email). Format to your taste. Order by date updated DESC.
9. (route: /product) Handle the pagination of results (10 per page).
10. (route: /product) Add a way to delete a product directly from the list of products.
11. Add a way to edit a product.

## Freeform test

Choose one of the following test subjects:
- Create a new route to get the products in JSON format. Show the product name and price as well as the supplier name but hide the “email” and the “siret” fields. Paginate this route.
- Create a service that can check if a company behind a SIRET number is still in business. Use it to verify the user input on your Product creation FormType. You can use the [Sirene API](https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/pages/item-info.jag?name=Sirene&version=V3&provider=insee) to check those numbers.
- Create a Category entity. A category can relate to itself and have a single parent and several children categories. Some categories are root (they have no parent). A product can be linked to a single category. Upgrade the `"/product"` page with a menu of root categories that show every product they contain (including sub-categories) when clicked.
