<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return null|int|mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->select($qb->expr()->count('p'))
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @param int $page_range
     * @param null|int $page
     * @return Collection|Product[]
     */
    public function getAll(int $page_range = 10, ?int $page = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select(['p', 's'])
            ->innerJoin('p.supplier', 's')
            ->orderBy('p.updatedAt', 'DESC')
        ;

        if ($page && $page_range) {
            $offSet = ($page - 1) * $page_range;
            $limit = $page_range;

            $qb->setFirstResult($offSet)
                ->setMaxResults($limit)
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
            ;
    }
}
