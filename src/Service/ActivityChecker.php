<?php


namespace App\Service;



use Symfony\Component\PropertyAccess\PropertyAccessorInterface;


class ActivityChecker
{
    /**
     * @var SireneHttpManager
     */
    private $sireneHttpManager;
    /**
     * @var PropertyAccessorInterface
     */
    private $accessor;

    public function __construct(SireneHttpManager $sireneHttpManager, PropertyAccessorInterface $accessor)
    {

        $this->sireneHttpManager = $sireneHttpManager;
        $this->accessor = $accessor;
    }

    /**
     * @param string $siret
     * @return bool
     */
    public function isStillInBusiness(string $siret): bool
    {
        $infos = $this->sireneHttpManager->getRequest($siret);

        if (!$infos) {
            return false;
        }
        $etablissement = $this->accessor->isReadable($infos, 'etablissement') ?
            $infos->etablissement :
            [];

        $periodeEtablissement = current($this->accessor->isReadable($etablissement, 'periodesEtablissement') ?
            $etablissement->periodesEtablissement:
            []);

        $dateFin = $this->accessor->isReadable($periodeEtablissement, 'dateFin') ?
            $periodeEtablissement->dateFin :
            null;

        return $this->accessor->isReadable($periodeEtablissement, 'dateFin') &&
            (
                !$dateFin ||
                ($dateFin > (new \DateTimeImmutable())->format('Y-m-d'))
            );
    }
}