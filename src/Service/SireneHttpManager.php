<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SireneHttpManager
{
    /**
     * @var HttpClientInterface
     */
    private $client;
    private $sireneToken;
    private $sireneEndpoint;

    public function __construct(HttpClientInterface $client, $sireneToken, $sireneEndpoint)
    {
        $this->client = $client;
        $this->sireneToken = $sireneToken;
        $this->sireneEndpoint = $sireneEndpoint;
    }

    /**
     * @param string $siret
     * @return null|object
     */
    public function getRequest(string $siret): ?object
    {
        try {
            $url = $this->getEndPoint('/siret/') . $siret;
            $bearerToken = $this->getBearerToken();
            $requestData = [
                'headers' => [
                    'Authorization' => $bearerToken,
                    'Accept' => 'application/json',
                ],
            ];
            $request = $this->client->request('GET', $url, $requestData);

            if (Response::HTTP_OK !== $request->getStatusCode()) {
                return null;
            }

            return json_decode($request->getContent());
        } catch (ExceptionInterface $e) {

            return null;
        }
    }

    /**
     * @param string $endPoint
     *
     * @return string
     */
    private function getEndPoint(string $endPoint): string
    {
        return sprintf('%s%s', $this->sireneEndpoint, $endPoint);
    }

    /**
     * @return string
     */
    private function getBearerToken(): string
    {
        return sprintf('Bearer %s', $this->sireneToken);
    }
}