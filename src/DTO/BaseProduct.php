<?php


namespace App\DTO;

use App\Entity\Supplier;
use App\Validator\SameAs;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BaseProduct
 */
abstract class BaseProduct
{
    /**
     * @var string
     * @SameAs(entity="App\Entity\Product")
     */
    public $name;

    /**
     * @var float
     * @SameAs(entity="App\Entity\Product")
     */
    public $price;

    /**
     * @var null|Supplier
     */
    public $existingSupplier;

    /**
     * @var bool
     *
     * @Assert\NotNull()
     * @Assert\AtLeastOneOf({
     *     @Assert\IsTrue(),
     *     @Assert\IsFalse()
     * })
     */
    public $isSupplierNew = false;

    /**
     * @var null|Supplier
     */
    public $newSupplier;
}