<?php


namespace App\DTO;


use App\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateProduct
 * @Assert\Callback({"App\Validator\DTOProductValidator", "validate"})
 */
class CreateProduct extends BaseProduct
{
    public function toEntity(): Product
    {
        return (new Product())
            ->setName($this->name)
            ->setPrice($this->price)
            ->setSupplier($this->isSupplierNew ? $this->newSupplier : $this->existingSupplier)
        ;
    }

}