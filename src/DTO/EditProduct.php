<?php


namespace App\DTO;


use App\Entity\Product;
use App\Entity\Supplier;
use App\Validator\SameAs;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EditProduct
 * @Assert\Callback({"App\Validator\DTOProductValidator", "validate"})
 */
class EditProduct extends BaseProduct
{
    /**
     * @var Product
     */
    public $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->name = $product->getName();
        $this->price = $product->getId();
        $this->existingSupplier = $product->getSupplier();
    }

    public function toEntity(): Product
    {
        return $this->product
            ->setName($this->name)
            ->setPrice($this->price)
            ->setSupplier($this->isSupplierNew ? $this->newSupplier : $this->existingSupplier)
        ;
    }



}