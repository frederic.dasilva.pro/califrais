<?php

namespace App\Validator;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintViolationList;

class SameAsValidator extends ConstraintValidator
{
    /**
     * @var Reader
     */
    private $annotationReader;

    public function __construct(Reader $annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    public function validate($value, Constraint $constraint): void
    {
        /** @var SameAs $constraint */
        $fieldName = $this->context->getPropertyName();
        $classEntity = $constraint->getEntity();

        if (!class_exists($classEntity)) {
            return;
        }

        $class = new \ReflectionClass($classEntity);
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            if ($fieldName === $property->getName()) {
                $annotations = $this->annotationReader->getPropertyAnnotations($property);
                foreach ($annotations as $annotation) {
                    if ($annotation instanceof Constraint) {
                        /** @var ConstraintViolationList $errors */
                        $errors = $this->context->getValidator()->validate($value, $annotation);
                        if ($errors->count() > 0) {
                            $errorsString = $errors->get(0)->getMessage();
                            $this->context->addViolation($errorsString);
                        }
                    }
                }
            }
        }
    }
}
