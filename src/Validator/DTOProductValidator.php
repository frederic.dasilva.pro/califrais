<?php


namespace App\Validator;


use App\DTO\BaseProduct;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DTOProductValidator
{
    public static function validate($object, ExecutionContextInterface $context, $payload): void
    {
        if (!($object instanceof BaseProduct)) {
            return;
        }

        $contextualValidator = $context->getValidator()->inContext($context);
        if (false === $object->isSupplierNew) {
            $contextualValidator
                ->atPath('existingSupplier')
                ->validate($object->existingSupplier, [
                    new NotBlank(),
                    new Valid()
                ])
            ;
        }

        if (true === $object->isSupplierNew) {
            $contextualValidator
                ->atPath('newSupplier')
                ->validate($object->newSupplier, [
                    new NotBlank(),
                    new Valid()
                ])
            ;
        }
    }
}