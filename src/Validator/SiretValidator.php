<?php

namespace App\Validator;

use App\Service\ActivityChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SiretValidator extends ConstraintValidator
{
    /**
     * @var ActivityChecker
     */
    private $activityChecker;

    public function __construct(ActivityChecker $activityChecker)
    {

        $this->activityChecker = $activityChecker;
    }

    public function validate($value, Constraint $constraint): void
    {
        /* @var $constraint Siret */
        if (empty($value) || !\IsoCodes\Siret::validate($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }

        // Check isStillInBusiness only if siret not empty and Luhn check also ok
        // I commented this part because the http call is too slow on my machine (it's weird)
        // You can disable it to check it on your side.
//        if (!empty($value) && \IsoCodes\Siret::validate($value) && !$this->activityChecker->isStillInBusiness($value)) {
//            $this->context->buildViolation('The company behind this SIRET number {{ value }} is not in business anymore.')
//                ->setParameter('{{ value }}', $value)
//                ->addViolation();
//        }


    }
}
