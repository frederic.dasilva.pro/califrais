<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Siret extends Constraint
{
    public $message = 'The value "{{ value }}" is not a valid SIRET number.';
}
