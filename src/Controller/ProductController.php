<?php

namespace App\Controller;

use App\DTO\CreateProduct;
use App\DTO\EditProduct;
use App\Entity\Product;
use App\Form\DTO\CreateProductType;
use App\Form\DTO\EditProductType;
use App\Repository\ProductRepository;
use App\Service\ActivityChecker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var ActivityChecker
     */
    private $activityChecker;

    public function __construct(ProductRepository $productRepository, EntityManagerInterface $manager, ActivityChecker $activityChecker)
    {
        $this->productRepository = $productRepository;
        $this->manager = $manager;
        $this->activityChecker = $activityChecker;
    }

    /**
     * @Route("/{page<\d+>?1}", name="product_index", methods={"GET"})
     * @param int|null $page
     * @return Response
     */
    public function index(?int $page): Response
    {
        $page_range = 10;
        $total = $this->productRepository->countAll();
        $nbPages = (int) ceil($total / $page_range);
        $rangeMin = $page - $page_range > 0 ? $page - $page_range : 1;
        $rangeMax = $page + $page_range < $nbPages ? $page + $page_range : $nbPages;
        $products = $this->productRepository->getAll($page_range, $page);
        return $this->render('product/list.html.twig', [
            'products' => $products,
            'nbPages' => $nbPages,
            'page' => $page,
            'rangeMin' => $rangeMin,
            'rangeMax' => $rangeMax,
        ]);
    }

    /**
     * @Route("/new", name="product_new", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $createProduct = new CreateProduct();
        $form = $this->createForm(CreateProductType::class, $createProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newProduct = $createProduct->toEntity();
            $this->manager->persist($newProduct);
            $this->manager->flush();
            $this->addFlash('success', sprintf('New product "%s" successfully created', $newProduct->getName()));
            return $this->redirectToRoute('product_index');
        }


        return $this->render('product/create_edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="product_edit", methods={"GET", "POST"})
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function edit(Product $product, Request $request): Response
    {
        $editProduct = new EditProduct($product);
        $form = $this->createForm(EditProductType::class, $editProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $existingProduct = $editProduct->toEntity();
            if (!$existingProduct->getSupplier()->getId()) {
                $this->manager->persist($existingProduct);
            }
            $this->manager->flush();
            $this->addFlash('success', sprintf('Product "%s" successfully edited', $product->getName()));
            return $this->redirectToRoute('product_index');
        }


        return $this->render('product/create_edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="product_delete", methods={"GET"})
     * @param Product $product
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param SessionInterface $session
     * @return Response
     */
    public function delete(Product $product, Request $request, EntityManagerInterface $manager, SessionInterface $session): Response
    {
        $isCsrfTokenValid = $this->isCsrfTokenValid('delete' . $product->getId(), $request->get('_csrf_token'));

        if (!$isCsrfTokenValid) {
            $this->addFlash('error', 'You do not have the right to delete this product.');
            throw $this->createAccessDeniedException('Token CSRF invalide');
        }


        $manager->remove($product);
        $manager->flush();
        $this->addFlash('success', sprintf('The product %s has been successfully removed.', $product->getName()));
        $previousRoute = $request->headers->get('referer');

        return $this->redirect($previousRoute);
    }
}
