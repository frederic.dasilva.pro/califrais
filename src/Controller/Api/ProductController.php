<?php

namespace App\Controller\Api;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/product")
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/{page<\d+>?1}", name="api_product_list", methods={"GET"})
     * @param int|null $page
     * @param Request $request
     * @return Response
     */
    public function index(?int $page, Request $request): Response
    {
        $page_range = 10;
        $products = $this->productRepository->getAll($page_range, $page);

        return $this->json(
            $products,
            JsonResponse::HTTP_OK,
            [],
            ['groups' => 'list_product']
        );
    }

}
