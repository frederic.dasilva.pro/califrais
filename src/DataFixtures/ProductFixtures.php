<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Repository\SupplierRepository;
use Bezhanov\Faker\Provider\Food;
use Bezhanov\Faker\ProviderCollectionHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    private const NB_INSTANCE = 100;
    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('en_US');
        $faker->addProvider(new Food($faker));
        ProviderCollectionHelper::addAllProvidersTo($faker);
        $suppliers = $this->supplierRepository->findAll();

        $populator = new Populator($faker, $manager);
        $populator->addEntity(Product::class, self::NB_INSTANCE, [
            'name' => function () use ($faker) {
                return $faker->ingredient;
            },
            'price' => function () use ($faker) {
                return $faker->randomFloat(2, 0, 20);
            },
            'supplier' => function () use ($faker, $suppliers) {
                return $faker->randomElement($suppliers);
            },
            'updatedAt' => null,
        ]);
        $populator->execute();

    }

    public function getDependencies(): array
    {
        return [
            SupplierFixtures::class
        ];
    }
}
