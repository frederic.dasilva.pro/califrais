<?php

namespace App\DataFixtures;

use App\Entity\Supplier;
use Bezhanov\Faker\Provider\Commerce;
use Bezhanov\Faker\ProviderCollectionHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;

class SupplierFixtures extends Fixture
{
    private const NB_INSTANCE = 20;

    public function load(ObjectManager $manager): void
    {
        $fakerFR = Factory::create('fr_FR');
        $fakerUS = Factory::create('en_US');
        $fakerUS->addProvider(new Commerce($fakerUS));
        ProviderCollectionHelper::addAllProvidersTo($fakerUS);

        $populator = new Populator($fakerUS, $manager);

        $populator->addEntity(Supplier::class, self::NB_INSTANCE, [
            'name' => function () use ($fakerUS) {
                return $fakerUS->company;
            },
            'email' => function () use ($fakerUS) {
                return $fakerUS->companyEmail;
            },
            'siret' => function () use ($fakerFR) {
                return str_replace(' ','', $fakerFR->siret);
            },
            'updatedAt' => null,
        ]);
        $populator->execute();

    }
}
