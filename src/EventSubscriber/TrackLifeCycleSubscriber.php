<?php

namespace App\EventSubscriber;

use App\Entity\Product;
use App\Entity\Supplier;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;

class TrackLifeCycleSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        if (!($object instanceof Product || $object instanceof Supplier)) {
            return;
        }
        $object->setCreatedAt(new \DateTimeImmutable());
    }

    public function preUpdate(LifecycleEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        if (!($object instanceof Product || $object instanceof Supplier)) {
            return;
        }
        /** @var UnitOfWork $uow */
        $uow = $eventArgs->getObjectManager()->getUnitOfWork();
        $changeSet = $uow->getEntityChangeSet($object);
        if (!empty($changeSet)) {
            $object->setUpdatedAt(new \DateTimeImmutable());
        }


    }
}
