<?php

namespace App\Form\DTO;

use App\DTO\EditProduct;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditProductType extends CreateProductType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EditProduct::class,
        ]);
    }
}
