<?php

namespace App\Form\DTO;

use App\DTO\CreateProduct;
use App\Entity\Supplier;
use App\Form\SupplierType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('price', MoneyType::class, [
                'scale' => 2,
            ])
            ->add('existingSupplier', EntityType::class, [
                'expanded' => false,
                'multiple' => false,
                'class' => Supplier::class,
                'placeholder' => 'Select a supplier',
                'required' => false,
            ])
            ->add('isSupplierNew', ChoiceType::class, [
                'label' => 'Create a new supplier',
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    'Yes' => true,
                    'No' => false,
                ],
                'data' => false,
            ])
            ->add('newSupplier', SupplierType::class, [
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreateProduct::class,
        ]);
    }
}
